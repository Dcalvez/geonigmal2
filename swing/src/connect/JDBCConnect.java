package connect;
import java.sql.*;
import java.util.ArrayList;

/**
 * Classe permettant l'accès à une base de données.<br/><br/>
 * 
 * Contient les méthodes : <br/>
 * <ul>
 * 	<li>public ArrayList String readBDD(String _colonne, String _table)</li>
 * 	<li>public ArrayList String readBDD(String _colonne, String _table, String _stringId,int _id)</li>
 * </ul>
 * 
 * @author dinnis
 *
 */
public class JDBCConnect {
	private  String uri = "jdbc:mysql://localhost:3306/";
	private final String user = "root";
	private final String pswd = "password";
	
	
	/**
	 * Constructeur de la classe JDBCConnect.
	 * 
	 * @param _dataBaseName est le nom de la base de données à laquelle la connexion est souhaitée.
	 */
	public JDBCConnect(String _dataBaseName) {
		uri += _dataBaseName;
	}
	
	/**
	 * <strong>Méthode static</strong> de la classe abstraite JDBCConnect.<br/>
	 * Sert a accéder à la BDD de façon à la lire, et retourne ce qui à été lu. <strong>Peut servir pour plusieurs éléments</strong>.<br/>
	 * 
	 * @param _colonne est le nom de la ou des colonne(s) de la BDD voulue(s).
	 * @param _table est la table de la BDD où la ou les colonne(s) se situe(s) dans la BDD.
	 * @return <strong>Retourne une ArrayList de string</strong> contenant le ou les résultat(s) de la requête sql.
	 */
	public ArrayList<String> readBDD(String _colonne, String _table) {				//permet de retourner un tableau de chaines de caractères qui contient les champs de la colonne(_colonne) de la table(_table)_
		ArrayList<String> returnList = new ArrayList<String>();
		String req = "SELECT "+_colonne+" FROM "+_table+";";
		
		try {
			Class.forName("com.mysql.jdbc.Driver"); 										//chargement du driver
			Connection conn = DriverManager.getConnection(uri, user, pswd);					//création de la connexion à la base de données
			
			Statement stmt = conn.createStatement();										//preparation de la requête
			ResultSet res = stmt.executeQuery(req);											//exécution de la requête
			ResultSetMetaData rsmd = res.getMetaData();
			int nbColomn = rsmd.getColumnCount();
			
			while (res.next()) {															//navigation dans le resultat tant que res.next() différent false
				for(int i = 1; i <= nbColomn; i++) {
					returnList.add(res.getString(i));										//remplissage de la liste de retour	
				}											
			}
			
			conn.close();																	//fermeture de la connexion à la base de données
			return returnList;																//retour du tableau
			
		} catch (Exception e) {																//si erreur de connexion à la base de données
			System.out.println(e);								
			return null;																	//si erreur de connexion retourne null
		}
	}

	/**
	 * <strong>Méthode static</strong> de la classe abstraite JDBCConnect.<br/>
	 * <strong>Permet d'accéder à un ou plusieurs éléments répondant à la condition</strong> de la BDD.<br/>
	 * 
	 * @param _colonne est le nom de la colonne de la BDD voulue.
	 * @param _table est la table de la BDD où la colonne se situe dans la BDD.
	 * @param _condition du WHERE de la requête sql si null retourne toute(s) la ou les colonnes spécifiées.
	 * @return <strong>Retourne une ArrayList de String</strong> contenant le résulats de la requête sql.
	 */
	public ArrayList<String> readBDD(String _colonne, String _table, String _condition) {	//permet de retourner une chaines de caractères qui contient le champs de la colonne(_colonne) de la table(_table)_ à l'indice(_id) dont le nom de cet indice est spécifié(_stringId)
		ArrayList<String> returnList = new ArrayList<String>();
		
		if(_condition != null) {
			String req = "SELECT "+_colonne+" FROM "+_table+_condition+";";
					
					try {
						Class.forName("com.mysql.jdbc.Driver"); 										//chargement du driver
						Connection conn = DriverManager.getConnection(uri, user, pswd);					//création de la connexion à la base de données
						
						Statement stmt = conn.createStatement();										//preparation de la requête
						ResultSet res = stmt.executeQuery(req);											//exécution de la requête
			
						ResultSetMetaData rsmd = res.getMetaData();
						int nbColomn = rsmd.getColumnCount();
						
						while (res.next()) {															//navigation dans le resultat tant que res.next() différent false
							for(int i = 1; i <= nbColomn; i++) {
								returnList.add(res.getString(i));										//remplissage de la liste de retour	
							}											
						}
						
						conn.close();																	//fermeture de la connexion à la base de données
						return returnList;															//retour du de la chaine de caractère
						
					} catch (Exception e) {																//si erreur de connexion à la base de données
						System.out.println(e);								
						return null;																	//si erreur de connexion retourne null
					}			
		}
		
		else return readBDD(_colonne, _table);
		
	}

	 /** <strong>MÃ©thode static</strong> de la classe abstraite JDBCConnect.<br/>
	  * <strong>Permet d'ajouter des données</strong> à la BDD.<br/>
	  *
	  * @param _table est la table de la BDD qui va être modifié.
	 * @param _valeur correspond aux valeur que l'on va ajouter à chaque ligne.
	  */

	public void insertBDD(String _table, String _valeur) {									//permet d'ajouter des données à la BDD.
		String req= "INSERT INTO "+_table+" VALUES("+_valeur+")";							//il faut autant de termes dans _valeur qu'il y a de colonne dans la table
																							//les termes de _valeur sont écrit de la manière suivante : 'test'
																							//lorsqu'il y a plusieurs termes ajouter une virgule entre chaque.
		try {
			Class.forName("com.mysql.jdbc.Driver"); 										//chargement du driver
			Connection conn = DriverManager.getConnection(uri, user, pswd);					//crÃ©ation de la connexion Ã  la base de donnÃ©es

			conn.createStatement().executeUpdate(req);										//préparation de la requète et mise à jour de la base de donnée
			conn.close();
		}
		catch (Exception e) {																//si erreur de connexion Ã  la base de donnÃ©es
			System.out.println(e);
		}
	}
}

