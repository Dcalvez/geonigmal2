package vue.elementGraphique;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;

public class bouton extends JButton{
	private int indexQt;
	
	public bouton(String _txtBp) {
		 super(_txtBp);
		 this.setBackground(Color.LIGHT_GRAY);
		 this.setBorderPainted(false);
	}

	public bouton(String _txtBp, int _index) {
		super(_txtBp);
		indexQt = _index;
		this.setBackground(Color.LIGHT_GRAY);
		this.setBorderPainted(false);
	}
	
	public int getIndexQt() {
		return indexQt;
	}
}
