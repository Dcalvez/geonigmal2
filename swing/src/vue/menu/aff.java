package vue.menu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.BorderLayout;
import java.awt.Font;

public class aff  extends JFrame{
	
	
	
	int cal;
	double num , ans;
	int nbtext=0;
	
	private JTextField textField;
	
	
	public aff() {
		
		
		
		
		 this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 this.setSize(1000, 550);
		 this.setLocationRelativeTo(null);
		 getContentPane().setLayout(null);
		 
		 JPanel fonc = new JPanel();
		 fonc.setBounds(0, 0, 241, 370);
		 fonc.setBackground(Color.DARK_GRAY);
		 fonc.setForeground(Color.WHITE);
		 getContentPane().add(fonc);
		 fonc.setLayout(null);
		 
		 textField = new JTextField();
		 textField.setBounds(6, 47, 229, 37);
		 fonc.add(textField);
		 textField.setColumns(10);
		 
		//************************************************//
			//LES DIFFERENTS PANEL//	
	//************************************************//
		 JPanel calcu = new JPanel();
		 calcu.setBounds(0, 369, 1000, 159);
		 getContentPane().add(calcu);
		 calcu.setLayout(null);
		 
		 JPanel panel = new JPanel();
		 panel.setBackground(Color.ORANGE);
		 panel.setBounds(0, 0, 70, 164);
		 calcu.add(panel);
		 
		 JPanel graph = new JPanel();
		 graph.setBounds(241, 0, 788, 370);
		 graph.setBackground(Color.WHITE);
		 getContentPane().add(graph);
		
		 
		 
		 
		 
		 
		 
		//************************************************//
			//BUTTON DE LA CALCULATRICE//	
	//************************************************//
		
		 
		 JButton button_7 = new JButton("+");
		 button_7.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		num = Double.parseDouble(textField.getText());
					cal = 1;
					textField.setText("");
			 	}
			 });
		 button_7.setBounds(171, 6, 64, 27);
		 fonc.add(button_7);
		 
		 
		 
		 JButton button_2 = new JButton("7");
		 button_2.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"7");
			 	}
			 });
		 button_2.setBounds(527, 5, 70, 35);
		 calcu.add(button_2);
		 
		 JButton button = new JButton("4");
		 button.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"4");
			 	}
			 });
		 button.setBounds(527, 40, 70, 35);
		 calcu.add(button);
		 
		 JButton button_1 = new JButton("1");
		 button_1.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"1");
			 	}
			 });
		 button_1.setBounds(527, 75, 70, 35);
		 calcu.add(button_1);
		 
		 JButton button_3 = new JButton("0");
		 button_3.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		textField.setText(textField.getText()+"0");
		 	}
		 });
		 button_3.setBounds(527, 110, 218, 35);
		 calcu.add(button_3);
		 
		 JButton button_4 = new JButton("8");
		 button_4.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"8");
			 	}
			 });
		 button_4.setBounds(601, 5, 70, 35);
		 calcu.add(button_4);
		 
		 JButton button_5 = new JButton("5");
		 button_5.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"5");
			 	}
			 });
		 button_5.setBounds(601, 40, 70, 35);
		 calcu.add(button_5);
		 
		 JButton button_6 = new JButton("2");
		 button_6.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"2");
			 	}
			 });
		 button_6.setBounds(601, 75, 70, 35);
		 calcu.add(button_6);
		 
		 JButton button_8 = new JButton("9");
		 button_8.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_8.setBounds(675, 5, 70, 35);
		 calcu.add(button_8);
		 
		 JButton button_9 = new JButton("6");
		 button_9.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"6");
			 	}
			 });
		 button_9.setBounds(675, 40, 70, 35);
		 calcu.add(button_9);
		 
		 JButton button_10 = new JButton("3");
		 button_10.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"3");
			 	}
			 });
		 button_10.setBounds(675, 75, 70, 35);
		 calcu.add(button_10);
		 
		 JButton button_12 = new JButton("+");
		 button_12.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		num = Double.parseDouble(textField.getText());
					cal = 1;
					textField.setText("");
			 	}
			 });
		 button_12.setBounds(751, 5, 70, 35);
		 calcu.add(button_12);
		 
		 JButton button_13 = new JButton("-");
		 button_13.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		num = Double.parseDouble(textField.getText());
					cal = 2;
					textField.setText("");
			 	}
			 });
		 button_13.setBounds(751, 40, 70, 35);
		 calcu.add(button_13);
		 
		 JButton button_14 = new JButton(".");
		 button_14.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+".");
			 	}
			 });
		 button_14.setBounds(751, 75, 70, 35);
		 calcu.add(button_14);
		 
		 JButton button_15 = new JButton("=");
		 button_15.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		calcul();
			 	}
			 });
		 button_15.setBounds(751, 110, 143, 35);
		 calcu.add(button_15);
		 
		 JButton button_16 = new JButton("Ã·");
		 button_16.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		num = Double.parseDouble(textField.getText());
					cal = 3;
					textField.setText("");
			 	}
			 });
		 button_16.setBounds(824, 5, 70, 35);
		 calcu.add(button_16);
		 
		 JButton btnX = new JButton("X");
		 btnX.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		num = Double.parseDouble(textField.getText());
					cal = 4;
					textField.setText("");
			 	}
			 });
		 btnX.setBounds(824, 40, 70, 35);
		 calcu.add(btnX);
		 
		 JButton btnAc = new JButton("AC");
		 btnAc.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		textField.setText(" ");
		 	}
		 });
		 btnAc.setBounds(824, 75, 70, 35);
		 calcu.add(btnAc);
		 
		 JButton btnX_1 = new JButton("x");
		 btnX_1.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"x");
			 	}
			 });
		 btnX_1.setBounds(215, 5, 70, 35);
		 calcu.add(btnX_1);
		 
		 JButton button_21 = new JButton("<");
		 button_21.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_21.setBounds(215, 40, 70, 35);
		 calcu.add(button_21);
		 
		 JButton button_22 = new JButton(">");
		 button_22.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_22.setBounds(215, 75, 70, 35);
		 calcu.add(button_22);
		 
		 JButton button_23 = new JButton("âˆš");
		 button_23.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_23.setBounds(215, 110, 70, 35);
		 calcu.add(button_23);
		 
		 JButton button_24 = new JButton("e");
		 button_24.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_24.setBounds(289, 110, 70, 35);
		 calcu.add(button_24);
		 
		 JButton button_25 = new JButton("âˆ�");
		 button_25.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_25.setBounds(363, 110, 70, 35);
		 calcu.add(button_25);
		 
		 JButton button_26 = new JButton(")");
		 button_26.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_26.setBounds(363, 75, 70, 35);
		 calcu.add(button_26);
		 
		 JButton button_27 = new JButton("â‰¥");
		 button_27.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_27.setBounds(289, 75, 70, 35);
		 calcu.add(button_27);
		 
		 JButton button_28 = new JButton("â‰¤");
		 button_28.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_28.setBounds(289, 40, 70, 35);
		 calcu.add(button_28);
		 
		 JButton button_29 = new JButton("(");
		 button_29.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_29.setBounds(363, 40, 70, 35);
		 calcu.add(button_29);
		 
		 JButton button_30 = new JButton("y");
		 button_30.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_30.setBounds(289, 5, 70, 35);
		 calcu.add(button_30);
		 
		 JButton button_31 = new JButton("z");
		 button_31.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		textField.setText(textField.getText()+"9");
			 	}
			 });
		 button_31.setBounds(363, 5, 70, 35);
		 calcu.add(button_31);
		 
		 
		
		

	}
	

	//************************************************//
		//ALGO DES OPERATIONS//	
	//************************************************//
	
	public void calcul() {
		
		switch(cal) {
		
			case 1://ADDITION
				ans = num + Double.parseDouble(textField.getText());
				textField.setText(Double.toString(ans));
				break;
			
			case 2://SOUSTRATION
				ans = num - Double.parseDouble(textField.getText());
				textField.setText(Double.toString(ans));
				break;
				
			case 3://DIVISION
				ans = num / Double.parseDouble(textField.getText());
				textField.setText(Double.toString(ans));
				break;
				
			case 4://MULTIPLICATION
				ans = num * Double.parseDouble(textField.getText());
				textField.setText(Double.toString(ans));
				break;
		}
	}
	
	
	
	
	
	//public static void main(String[] args) throws UnsupportedLookAndFeelException  {
		// TODO Auto-generated method stub
		//UIManager.setLookAndFeel(new NimbusLookAndFeel());
		//aff a = new aff();
		//a.setVisible(true);
	//}
}
