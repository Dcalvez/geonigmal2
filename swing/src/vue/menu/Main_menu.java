package vue.menu;

import vue.Scene_Principale;
import vue.popUpFrame.AddQuestionForm;
import vue.repere.Fenetre;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

public class Main_menu extends Menu {
	
	public Main_menu(Scene_Principale sc) {
		super(sc);
		
		getT().setLayout(null);
		//On cr�e un deux boutons un pour jouer l'autre pour quitter
		JLabel titre = new JLabel("Geonigma"); //On cr�e le titre
		getCentre().setLayout(null);  //On d�finit la forme de la fen�tre � null
		JButton a = new JButton("Jouer");
	    JButton b = new JButton("Quitter");
	    JButton c = new JButton("Editeur Question");
	    JButton d = new JButton("Cours");
	    JButton e = new JButton("Rep�re Cart�sien");
	    
	    titre.setLocation(450, 100);
	    titre.setSize(800,200);
	    titre.setFont(new Font("Arial", Font.BOLD, 125));
	     
	    a.setBounds(650,330,150,50);
	    b.setBounds(650,730,150,50);
	    c.setBounds(650,430,150,50);
	    d.setBounds(650,530,150,50);
	    e.setBounds(650,630,150,50);
	    
	    
	    
	    // ajoute au conteneur les �l�ments ci dessus
	    getCentre().add(titre);
	    getCentre().add(a);
	    getCentre().add(b);
	    getCentre().add(c);
	    getCentre().add(d);
	    getCentre().add(e);
        
        
       //action listener: v�rifie si on a appuy� sur le bouton et r�agit
       a.addActionListener(new ActionListener() {
       	public void actionPerformed(ActionEvent e) {
       		//remplace l'ancien Jpanel par celui des questions
       		Questions qt=new Questions(getSc());
       		getSc().switchScene(qt);
       		
       			
       	}
       });   
       
       b.addActionListener(new ActionListener() {
          	public void actionPerformed(ActionEvent e) {
          		getSc().setVisible(false);
          		getSc().dispose();
          			
          	}
          });
		
	
	
	c.addActionListener(new ActionListener() {
      	public void actionPerformed(ActionEvent e) {
      		AddQuestionForm add = new AddQuestionForm();
      			
      	}
      });
	
	d.addActionListener(new ActionListener() {
       	public void actionPerformed(ActionEvent e) {
       		
       		ArrayList<String> liste_cours = new ArrayList<String>();
      		
      		liste_cours.add("Geometrie");
      		liste_cours.add("Algebre");
      		liste_cours.add("Probas et Stats");
      		liste_cours.add("Analyse");


       		Choix_cours c=new Choix_cours(sc,liste_cours);
       		getSc().switchScene(c);
       		
       			
       	}
       });
	
	e.addActionListener(new ActionListener() {
       	public void actionPerformed(ActionEvent e) {
       		//remplace l'ancien Jpanel par celui des questions
       		Fenetre a = new Fenetre(sc);
       		//aff2 a= new aff2(sc);
       		getSc().switchScene(a);
       		
       		
       			
       	}
       });
	
}
	
	

}
