package vue.menu;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;

import vue.menu.*;
import vue.Scene_Principale;

public class Choix_cours extends Menu implements ActionListener {
	
	private ArrayList<String> liste_cours = new ArrayList<String>();
	private ArrayList<JButton> liste_bouton = new ArrayList<JButton>();
	private int i=0;
	
	public Choix_cours(Scene_Principale _sc, ArrayList<String> a ) {
		
		super(_sc);
		getT().setLayout(null);
		
		//On creer les listes qui vont contenir les noms des cours et les boutons
		
		
		
		int n=0;
		int x =120,y=100;
		
		liste_cours=a;
		
		
	     
		//permet de placer les bouton avec x et y
	    
	    
	    
	    //On determine le nombre de cours
	    
	    for (i=0; i <liste_cours.size();i++) {
	    }
	    
	    //on cree les boutons
	    
	    for (n=0;n<i;n++) {
	    	
	    	liste_bouton.add(new JButton(liste_cours.get(n)));
	    	liste_bouton.get(n).setBounds(x,y,180,60);
	    	
	    	x=x+200;
	    	if ((n+1)%7==0) {
	    		x=x-1400;
	    		y=y+200;
	    	}
	    	
	    	liste_bouton.get(n).addActionListener(this);
	    	getT().add(liste_bouton.get(n));
	    	
	    	
	    }
	    
	    
	    
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		int n;
		for (n = 0; n < i; n++) {

			if (e.getSource().equals(liste_bouton.get(n))) {
				
				//Partie test qui sera remplacer par la base de donn�e//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				ArrayList<ArrayList> liste_cours = new ArrayList<ArrayList>();
				
				ArrayList<String> Geometrie = new ArrayList<String>();
				
				Geometrie.add("Les Ellipses");
				Geometrie.add("Geometrie dans l'espace");
				Geometrie.add("Sections");
				Geometrie.add("Polygones");
				
				
				liste_cours.add(Geometrie);
				
				
				ArrayList<String> Algebre = new ArrayList<String>();
				
				Algebre.add("Polynomes");
				Algebre.add("Matrices");
				Algebre.add("LCI, Groupes,Corps");
				Algebre.add("Boole");
				Algebre.add("Ensembles");
				
				liste_cours.add(Algebre);
				
				ArrayList<String> Stats  = new ArrayList<String>();
				
				Stats.add("Bernouilli");
				Stats.add("Poisson,Gauss");
				Stats.add("Intervalles de confiance");
				Stats.add("Medianes,moyennes");
				
				liste_cours.add(Stats);
				
				ArrayList<String> Analyse = new ArrayList<String>();
				
				Analyse.add("Derivees");
				Analyse.add("Integrales");
				Analyse.add("Etudes de fonctions");
				Analyse.add("Complexes");
				Analyse.add("Suites");
		
				liste_cours.add(Analyse);
				
				
				ArrayList<ArrayList> liste_lien = new ArrayList<ArrayList>();
			
				
				ArrayList<String> y = new ArrayList<String>();
				y.add("http://www.google.fr");
				y.add("https://www.google.com");
				liste_lien.add(y);
				
				ArrayList<String> f = new ArrayList<String>();
				f.add("https://www.youtube.com");
				f.add("https://fr-fr.facebook.com/");
				liste_lien.add(f);
				
				ArrayList<String> g = new ArrayList<String>();
				g.add("https://www.games-workshop.com");
				g.add("https://www.games-workshop.com");
				liste_lien.add(g);
				
				ArrayList<String> o = new ArrayList<String>();
				o.add("https://www.games-workshop.com");
				o.add("https://www.games-workshop.com");
				liste_lien.add(o);
				
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
				liste_cours d=new liste_cours(sc,liste_cours.get(n),liste_lien.get(n));  ///on lance le menu liste cours
           		getSc().switchScene(d);
				break;
				
			}	
		}	
	}
	
	public int getI() {
		return i;
	}
	
	public ArrayList<JButton> getListe_bouton() {
		return liste_bouton;
	}

}
