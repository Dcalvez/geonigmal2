package vue.menu;

import vue.Scene_Principale;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;

import javax.swing.JPanel;
import javax.swing.border.Border;


import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame{
	
	JToolBar toolbar;
	JPanel centre;
	JPanel t;
	Scene_Principale sc;
	
	
	public Menu(Scene_Principale _sc) {
		toolbar = new JToolBar();
		sc=_sc;
		centre=new JPanel();
		
		t = new JPanel();
		centre.setLayout(new BorderLayout());
		centre.setSize(1600,900);
		centre.add(toolbar,BorderLayout.NORTH);
		centre.add(t);
		
		//specification de la toolbar
		toolbar.setBounds(0,0,1600,40);
		JButton home=new JButton("Accueil");
		toolbar.add(home);
		JButton questions=new JButton("Questions");
		toolbar.add(questions);
		JButton cours=new JButton("Cours");
		toolbar.add(cours);
		JButton repere=new JButton("Rep�re");
		toolbar.add(repere);
		Dimension d=new Dimension(100,100);
		toolbar.addSeparator(d);
		
		/*		
		  http://www.java2s.com/Tutorial/Java/0240__Swing/SwingToolBarwithImagebutton.htm
		  http://www.java2s.com/Tutorial/Java/0240__Swing/CustomizingJToolBarLookandFeel.htm
		 */
		home.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Main_menu m=new Main_menu(getSc());
				getSc().switchScene(m);
			}
		});
		questions.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Questions q=new Questions(getSc());
				getSc().switchScene(q);
			}
		});
		cours.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> liste_cours = new ArrayList<String>();
	      		
				liste_cours.add("Geometrie");
	      		liste_cours.add("Algebre");
	      		liste_cours.add("Probas et Stats");
	      		liste_cours.add("Analyse");


	       		Choix_cours c=new Choix_cours(sc,liste_cours);
	       		getSc().switchScene(c);
			}
		});
		repere.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				aff2 a= new aff2(sc);
	       		getSc().switchScene(a);
			}
		});
	}
	/**
	 * Permet d'obtenir le JPanel g�n�r� par un menu
	 * @author daniel
	 * @return JPanel
	 * */
	public JPanel getT() {
		return t;
	}
	
	/**
	 * Permet d'obtenir la fen�tre dans laquelle est contenue
	 * le JPanel
	 * @author daniel
	 * @return JPanel
	 * */
	public Scene_Principale getSc() {
		return sc;
	}
	public JPanel getCentre() {
		return centre;
	}
	public JToolBar getToolbar() {
		return toolbar;
	}
	
	
}


