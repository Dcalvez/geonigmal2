package vue;

import java.awt.Graphics;

import javax.swing.JPanel;

public class Dessiner extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paintComponent( Graphics g )
    {
		int x=Repere.getx();
		int y=Repere.gety();
        g.fillOval( x-10, y-25, 10, 10 );
        
    }

}
