package vue;

import java.awt.BorderLayout;
import java.util.concurrent.BrokenBarrierException;

import javax.swing.JPanel;
import javax.swing.border.Border;

public abstract class Menu {
	JPanel toolbar;
	JPanel centre;
	JPanel t;
	Scene_Principale sc;
	
	public Menu(Scene_Principale _sc) {
		toolbar = new JPanel();
		sc=_sc;
		centre=new JPanel();
		
		t = new JPanel();
		centre.setLayout(new BorderLayout());
		centre.setSize(1600,900);
		centre.add(toolbar);
		centre.add(t);
	}
	
	
	/**
	 * Permet d'obtenir le JPanel g�n�r� par un menu
	 * @author daniel
	 * @return JPanel
	 * */
	public JPanel getT() {
		return t;
	}
	
	/**
	 * Permet d'obtenir la fen�tre dans laquelle est contenue
	 * le JPanel
	 * @author daniel
	 * @return JPanel
	 * */
	public Scene_Principale getSc() {
		return sc;
	}
	
	public JPanel getCentre() {
		return centre;
	}

}
