package vue;

import vue.menu.Main_menu;
import vue.menu.Menu;

import javax.swing.JFrame;

public class Scene_Principale extends JFrame {
	
	public Scene_Principale(){ 
		
		//Definit le titre la taille et la position de la fen�tre et interdit de modifier sa taille
	    this.setTitle("Geonigma");
	    this.setSize(1600, 900);
	    this.setResizable(false);
	    this.setLocationRelativeTo(null);
	    this.setLayout(null);
	 
	  
	    //Termine le processus lorsqu'on clique sur la croix rouge
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    Main_menu menu = new Main_menu(this);
	    switchScene(menu);
          
	    this.setVisible(true);
	    
	   	   
	}
	/**
	 * Utilis�e pour switcher de scene principale
	 * @param JPanel
	 */
	public void switchScene(Menu pan) {
		this.setContentPane(pan.getCentre());
		this.repaint();
		this.revalidate();

	}
}
