package vue;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.*;
import vue.menu.bonne_reponse;
import vue.menu.mauvaise_reponse;

public class InterfQT implements ActionListener{
	private QuestionReq qtReq;
	private Scene_Principale sc;
	private int indice;
	private JPanel[] pan;
	private String texteQt;
	private List<String> reponses;
	private List<String> gdReponses;
	private JButton valider;
	private JCheckBox[] tabCk;
	private boolean[] bnRep;
	
	public InterfQT(Scene_Principale sc,int _indice, QuestionReq _rep) {
		
		//////////////////////////////////////////////////////////////
		this.sc =sc;
		qtReq = _rep;
		bnRep = new boolean[4];
		pan = new JPanel[2];
		pan[0] = new JPanel();
		pan[1] = new JPanel();
		indice = _indice;
		texteQt= qtReq.getQtList().get(indice);
		reponses = qtReq.getRepList().get(indice);
		gdReponses = qtReq.getBrList().get(indice);
		
		/////////////////////////////////////////////////////////////
		
		pan[0].add(new JLabel(texteQt));
		pan[0].setSize(1400,600);
		//////////////////////////////////////////////////////////////
		
		pan[1].setBackground(new Color(70,0,0));
		pan[1].setLayout(new FlowLayout());
		pan[1].setMinimumSize(new Dimension(1400,200));;
		
		tabCk = new JCheckBox[4];
		valider = new JButton("Valider");
		valider.addActionListener(this);
		valider.setSize(150,50);
		
		for (int i = 0; i < reponses.size(); i++) {
			JCheckBox ck = new JCheckBox(reponses.get(i));
			ck.setBackground(new Color(70,0,0));
			ck.setForeground(Color.WHITE);
			System.out.println("reponse a i = "+reponses.get(i));
			tabCk[i] = ck;
			tabCk[i].addActionListener(this);
			
	
			pan[1].add(tabCk[i]);
		}
		pan[1].add(valider);
	}

	
	public void actionPerformed(ActionEvent e) {
		ArrayList<String> checkList = new ArrayList<String>();
		int cpt = 0;
		int nbCheck = 0;
		boolean validate = false;
		if(e.getSource().equals(valider)) {
			for (int i = 0; i < 4; i++) {
				if (tabCk[i].isSelected()) {
					checkList.add(tabCk[i].getText());
					nbCheck++;
				}
			}
			
			if(nbCheck == gdReponses.size()) {
				for(int i = 0; i < nbCheck; i++) {
					if(gdReponses.indexOf(checkList.get(i)) != -1) cpt++;
				}
				
				if(cpt == gdReponses.size()) validate = true;
			}
			
			
			System.out.println(cpt);
			if (validate) {
				bonne_reponse rep = new bonne_reponse(sc);
				sc.switchScene(rep);
			}
			else{
				mauvaise_reponse rep = new mauvaise_reponse(sc);
				sc.switchScene(rep);
			}
		}
	}
	
	public JPanel[] getPan() {
		return pan;
	}
}
