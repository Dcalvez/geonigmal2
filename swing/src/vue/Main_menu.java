package vue;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class Main_menu extends Menu{
	
	public Main_menu(Scene_Principale sc) {
		super(sc);
		
		getT().setLayout(null);
		//On cr�e un deux boutons un pour jouer l'autre pour quitter
		JLabel titre = new JLabel("Geonigma"); //On cr�e le titre
		getCentre().setLayout(null);  //On d�finit la forme de la fen�tre � null
		JButton a = new JButton("Jouer");
	    JButton b = new JButton("Quitter");
	    titre.setLocation(350, 100);
	    titre.setSize(800,200);
	    titre.setFont(new Font("Arial", Font.BOLD, 125));
	     
	    a.setBounds(650,330,150,50);
	    b.setBounds(650,430,150,50);
	    
	    
	    // ajoute au conteneur les �l�ments ci dessus
	    getCentre().add(titre);
	    getCentre().add(a);
	    getCentre().add(b);
        
        
       //action listener: v�rifie si on a appuy� sur le bouton et r�agit
       a.addActionListener(new ActionListener() {
       	public void actionPerformed(ActionEvent e) {
       		//remplace l'ancien Jpanel par celui des questions
       		Questions qt=new Questions(getSc());
       		getSc().switchScene(qt);
       		
       			
       	}
       });   
       
       b.addActionListener(new ActionListener() {
          	public void actionPerformed(ActionEvent e) {
          		getSc().setVisible(false);
          		getSc().dispose();
          			
          	}
          });
		
	}
	
	

}
