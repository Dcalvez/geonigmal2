package vue.popUpFrame;

import javax.swing.*;
import java.awt.*;

public abstract class PopUpFrame extends JFrame {

    public PopUpFrame(String _frameName){
        super(_frameName);
        this.setSize(950, 475);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
    }
}
