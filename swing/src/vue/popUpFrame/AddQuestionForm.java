package vue.popUpFrame;

import connect.JDBCConnect;
import vue.springUtilities.SpringUtilities;
import vue.elementGraphique.bouton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe permettant l'affichage de l'interface graphique de l'ajout de question dans la base de données.
 */
public class AddQuestionForm extends PopUpFrame implements ActionListener {
    private JDBCConnect conn;                                                       //déclaration de la connexion à la base de données

    private JTextField newQuestion;                                                 //déclaration de la zone de texte de la nouvelle question
    private JLabel newQuestionLabel;                                                //déclaration du label de la zone de texte
    private bouton send;                                                            //déclaration du bouton send

    private JCheckBox[] tabCheckBox;                                                //déclaration d'un tableau de checkbox pour désigner les bonnes réponses
    private JPanel[] pan;                                                           //déclaration d'un tableau de label pour les nouvelles réponses
    private JTextField[] tabTextField;                                              //déclaration d'un tableau de zone de texte pour les nouvelles réponses
    private JLabel[] tabLabel;

    /**
     * Construit la fenêtre, puis créer les différents objets graphiques ainsi que les canaux d'écoute pour les actions effectuées sur ces éléments.
     */
    public AddQuestionForm(){
        super("Ajouter une question");                                  //appel du constructeur de la classe abstraite PopUpFrame

        conn = new JDBCConnect("geonigma");                          //instanciation de la connexion

        tabCheckBox = new JCheckBox[4];                                           //
        pan = new JPanel[2];                                                      // instanciation des différents objets graphiques
        tabTextField= new JTextField[4];                                          //
        tabLabel = new JLabel[4];                                                 //

        send = new bouton("Envoyer");                                      //instanciation de send
        send.addActionListener(this);                                          //ajout d'un actionListener sur send

        pan[0] = new JPanel();
        pan[1] = new JPanel();

        pan[0].setBackground(Color.LIGHT_GRAY);
        pan[1].setBackground(Color.GRAY);

        pan[0].setLayout(new SpringLayout());
        pan[1].setLayout(new FlowLayout());

        newQuestion = new JTextField();
        newQuestionLabel = new JLabel("Nouvelle question : ");
        newQuestionLabel.setLabelFor(newQuestion);

        newQuestion.setBorder(BorderFactory.createEmptyBorder());

        pan[0].add(newQuestionLabel);
        pan[0].add(newQuestion);

        int charInt = (int)('A');                                                                       // ajout des champs et labels pour les nouvelles réponses
        for(int i = 0; i < 4; i++){                                                                     //
            tabTextField[i] = new JTextField();                                                         //
            tabLabel[i] = new JLabel("Réponse "+(char)(charInt)+" : ");                            //
            tabLabel[i].setLabelFor(tabTextField[i]);                                                   //

            tabTextField[i].setBorder(BorderFactory.createEmptyBorder());                               //

            pan[0].add(tabLabel[i]);                                                                    //
            pan[0].add(tabTextField[i]);                                                                //
            charInt+=1;                                                                                 //
        }
        charInt = (int)('A');

        SpringUtilities.makeCompactGrid(pan[0], 5, 2, 6, 6, 5, 20);

        for(int i = 0; i < tabCheckBox.length; i++){
            tabCheckBox[i] = new JCheckBox((char)(charInt)+" est une bonne réponse.");
            tabCheckBox[i].setBackground(Color.GRAY);
            tabCheckBox[i].addActionListener(this);
            pan[1].add(tabCheckBox[i]);
            charInt+=1;
        }

        pan[1].add(send);

        this.getContentPane().add(pan[0], BorderLayout.CENTER);
        this.getContentPane().add(pan[1], BorderLayout.SOUTH);
        setVisible(true);
    }


    /**
     * Ajoute si les champs de textes ne sont pas vide et si il y au moins une bonne réponse de cochées, la question ainsi que ses bonnes réponses à la base de données.
     * @param e est l'évènement qui est récupéré.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(send)){
            int testOneCheck = 0;
            int lastQuestionId = 0;
            boolean allRepWrite = true;

            for(int i = 0; i < 4; i++){
                if(tabTextField[i].getText().equals("")) allRepWrite = false;
                if(tabCheckBox[i].isSelected()){
                    testOneCheck+=1;
                }
            }

            if(testOneCheck != 0){
                if(!newQuestion.getText().equals("") && allRepWrite == true){
                    conn.insertBDD("QUESTION (texteQuestion)", "'"+newQuestion.getText().replaceAll("'", "''")+"'");
                    lastQuestionId = Integer.parseInt(conn.readBDD("MAX(idQuestion)", "QUESTION").get(0));

                    for(int i = 0; i < 4; i++){
                        if(tabCheckBox[i].isSelected()) conn.insertBDD("REPONSE (idQuestion, texteReponse, gdReponse)", (lastQuestionId)+",'"+tabTextField[i].getText().replaceAll("'", "''")+"',"+1);
                        else conn.insertBDD("REPONSE (idQuestion, texteReponse, gdReponse)", (lastQuestionId)+",'"+tabTextField[i].getText().replaceAll("'", "''")+"',"+0);
                    }
                    this.dispose();
                }
                else JOptionPane.showMessageDialog(this, "Tous les champs doivent etre renseignes !");

            }
            else JOptionPane.showMessageDialog(this, "Il doit y avoir au moins une bonne reponnse !");
        }
    }
}
