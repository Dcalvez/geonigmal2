package vue;

import connect.JDBCConnect;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Classe permettant l'affichage de l'interface graphique de l'ajout de question dans la base de données.
 */
public class AddQuestionForm extends JFrame implements ActionListener {
    private JDBCConnect conn;

    private JTextField newQuestion;
    private JLabel newQuestionLabel;
    private bouton send;

    private JCheckBox[] tabCheckBox;
    private JPanel[] pan;
    private JTextField[] tabTextField;
    private JLabel[] tabLabel;

    /**
     * Construit la fenêtre, puis créer les différents objets graphiques ainsi que les canaux d'écoute pour les actions effectuées sur ces éléments.
     */
    public AddQuestionForm(){
        super("Ajouter une question");
        this.setSize(950, 475);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        conn = new JDBCConnect("geonigma");

        tabCheckBox = new JCheckBox[4];
        pan = new JPanel[2];
        tabTextField= new JTextField[4];
        tabLabel = new JLabel[4];

        send = new bouton("Envoyer");
        send.addActionListener(this);

        pan[0] = new JPanel();
        pan[1] = new JPanel();

        pan[0].setBackground(Color.LIGHT_GRAY);
        pan[1].setBackground(Color.GRAY);

        pan[0].setLayout(new SpringLayout());
        pan[1].setLayout(new FlowLayout());

        newQuestion = new JTextField();
        newQuestionLabel = new JLabel("Nouvelle question : ");
        newQuestionLabel.setLabelFor(newQuestion);

        newQuestion.setBorder(BorderFactory.createEmptyBorder());

        pan[0].add(newQuestionLabel);
        pan[0].add(newQuestion);

        int charInt = (int)('A');
        for(int i = 0; i < 4; i++){
            tabTextField[i] = new JTextField();
            tabLabel[i] = new JLabel("Réponse "+(char)(charInt)+" : ");
            tabLabel[i].setLabelFor(tabTextField[i]);

            tabTextField[i].setBorder(BorderFactory.createEmptyBorder());

            pan[0].add(tabLabel[i]);
            pan[0].add(tabTextField[i]);
            charInt+=1;
        }
        charInt = (int)('A');

        SpringUtilities.makeCompactGrid(pan[0], 5, 2, 6, 6, 5, 20);

        for(int i = 0; i < tabCheckBox.length; i++){
            tabCheckBox[i] = new JCheckBox((char)(charInt)+" est une bonne réponse.");
            tabCheckBox[i].setBackground(Color.GRAY);
            tabCheckBox[i].addActionListener(this);
            pan[1].add(tabCheckBox[i]);
            charInt+=1;
        }

        pan[1].add(send);

        this.getContentPane().add(pan[0], BorderLayout.CENTER);
        this.getContentPane().add(pan[1], BorderLayout.SOUTH);
        setVisible(true);
    }


    /**
     * Ajoute si les champs de textes ne sont pas vide et si il y au moins une bonne réponse de cochées, la question ainsi que ses bonnes réponses à la base de données.
     * @param e est l'évènement qui est récupéré.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(send)){
            int testOneCheck = 0;
            int lastQuestionId = 0;
            boolean allRepWrite = true;

            for(int i = 0; i < 4; i++){
                if(tabTextField[i].getText().equals("")) allRepWrite = false;
                if(tabCheckBox[i].isSelected()){
                    testOneCheck+=1;
                }
            }

            if(testOneCheck != 0){
                if(!newQuestion.getText().equals("") && allRepWrite == true){
                    conn.insertBDD("QUESTION (texteQuestion)", "'"+newQuestion.getText()+"'");
                    lastQuestionId = Integer.parseInt(conn.readBDD("MAX(idQuestion)", "QUESTION").get(0));

                    for(int i = 0; i < 4; i++){
                        if(tabCheckBox[i].isSelected()) conn.insertBDD("REPONSE (idQuestion, texteReponse, gdReponse)", (lastQuestionId)+",'"+tabTextField[i].getText()+"',"+1);
                        else conn.insertBDD("REPONSE (idQuestion, texteReponse, gdReponse)", (lastQuestionId)+",'"+tabTextField[i].getText()+"',"+0);
                    }
                }
                else JOptionPane.showMessageDialog(this, "Tous les champs doivent être renseignés !");

            }
            else JOptionPane.showMessageDialog(this, "Il doit y avoir au moins une bonne réponnse !");
        }
    }
}
