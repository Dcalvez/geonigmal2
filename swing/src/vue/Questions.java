package vue;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import connect.JDBCConnect;
import controleur.QuestionReq;

public class Questions extends Menu implements ActionListener {
	private JPanel[] pan;
	private JDBCConnect conn;
	private List<bouton> bp;
	private List<String> questions;
	private QuestionReq qRep;

	public Questions(Scene_Principale _sc) {
		super(_sc);
		qRep = new QuestionReq();
		getT().setLayout(new BorderLayout());
		pan = new JPanel[3];

		pan[0] = new JPanel();
		pan[1] = new JPanel();
		pan[2] = new JPanel();

		pan[0].setBackground(Color.BLACK);
		pan[0].setLayout(new SpringLayout());

		pan[1].setBackground(Color.GRAY);
		pan[1].setLayout(new SpringLayout());
		pan[1].setSize(200,800);

		conn = new JDBCConnect("geonigma");
		questions = conn.readBDD("idQuestion", "QUESTION");
		bp = new ArrayList<bouton>();
		
		
		pan[2].setBackground(Color.WHITE);
		pan[2].setLayout(new FlowLayout());
		pan[2].setSize(1400,200);
		
		// nombre de questions
		System.out.println(questions.size());
		int i = 0;
		while (i < questions.size()) {
			bp.add(new bouton("Q" + (i + 1), i));

			bp.get(i).addActionListener(this);
			pan[0].add(bp.get(i));
			i++;
		}
		SpringUtilities.makeCompactGrid(pan[0], questions.size(), 1, 6, 6, 6, 10);

		getT().add(pan[0], BorderLayout.WEST);
		getT().add(pan[2], BorderLayout.SOUTH);
		getT().add(pan[1], BorderLayout.CENTER);

		chgQt(0);

	}

	public void chgQt(int i) {

		InterfQT qt = new InterfQT(sc, i, qRep);
		pan[1].removeAll();
		pan[1].add(qt.getPan()[0]);
		pan[2].removeAll();
		pan[2].add(qt.getPan()[1]);
		pan[1].revalidate();
		pan[1].repaint();
		pan[2].revalidate();
		pan[2].repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for (int n = 0; n < questions.size(); n++) {

			if (e.getSource().equals(bp.get(n)))
				chgQt(n);
		}
	}
}
