package vue.repere;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

import vue.Scene_Principale;
import vue.menu.Menu;

public class Fenetre extends Menu {

	private FormeListener fListener = new FormeListener();
	private SituationListener sListener = new SituationListener();
	
	private ArrayList<Point> points = new ArrayList<Point>();
	private ArrayList<Droite> droites = new ArrayList<Droite>();
	private ArrayList<Cercle> cercle2 = new ArrayList<Cercle>();
	

//LE MENU 
	private JMenuBar menuBar = new JMenuBar();
	JMenu option = new JMenu("Options");
	JMenu probleme = new JMenu("Problemmes");
	JMenuItem nouveau = new JMenuItem("Tout Effacer"), 
	quitter = new JMenuItem("Quitter"),
	rond = new JMenuItem("Point"),
	cercle=new JMenuItem("Cercle"),
	droite = new JMenuItem("Droite"),
	effasser = new JMenuItem("Effacer"),
	p1 = new JMenuItem("Probleme 1"),
	p2 = new JMenuItem("Probleme 2"),
	p3 = new JMenuItem("Probleme 3");
	

	/**Notre zone de dessin*/
	Situation_ini i=new Situation_ini(points, droites, cercle2);
	
	private DrawPanel drawPanel = new DrawPanel();

	public Fenetre(Scene_Principale _sc) {
		super(_sc);
		getT().setLayout(new BorderLayout());
		
		

		/** On initialise le menu*/
		initMenu();
		
		
		/** On positionne notre zone de dessin*/
		drawPanel.setBackground(Color.black);
		drawPanel.setLayout(new SpringLayout());
		drawPanel.setSize(400,400);
		getT().add(drawPanel,BorderLayout.CENTER);
		getT().setVisible(true);

	}

/**Initialise le menu*/
	private void initMenu() {
		
		nouveau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				drawPanel.erase();
			}
		});

		nouveau.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));

		option.add(rond);
		option.add(droite);
		option.add(cercle);
		option.add(effasser);
		option.add(nouveau);
		option.setMnemonic('F');
		
		probleme.add(p1);
		probleme.add(p2);
		probleme.add(p3);
		probleme.setMnemonic('F');
		

		droite.addActionListener(fListener);
		rond.addActionListener(fListener);
		cercle.addActionListener(fListener);
		effasser.addActionListener(fListener);
		
		p1.addActionListener(sListener);
		p2.addActionListener(sListener);
		p3.addActionListener(sListener);

		menuBar.add(option);
		menuBar.add(probleme);
		this.setJMenuBar(menuBar);
		
		
		getT().add(menuBar,BorderLayout.NORTH);
		

	}

	class FormeListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (e.getSource().getClass().getName().equals("javax.swing.JMenuItem")) {
				if (e.getSource() == droite)
					drawPanel.setPointerType("droite");

				else if (e.getSource() == rond)
					drawPanel.setPointerType("point");
				else if (e.getSource() == effasser)
					drawPanel.setPointerType("effasser");
				else if(e.getSource() == cercle)
					drawPanel.setPointerType("cercle");
			}
		}
	}
	
	class SituationListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (e.getSource().getClass().getName().equals("javax.swing.JMenuItem")) {
				if (e.getSource() == p1)
					drawPanel.setSituation("p1");

				else if (e.getSource() == p2)
					drawPanel.setSituation("p2");
				
				else if(e.getSource() == p3)
					drawPanel.setSituation("p3");
				drawPanel.Situation();
			}
		}
	}

}