package vue.repere;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class DrawPanel extends JPanel {  

/**Couleur du pointeur*/
	private Color pointerColor = Color.black;
/**Forme du pointeur*/
	private String pointerType = "point";
/**situation initial*/
	private String situation = "";
/**Position X du pointeur*/
	private int posX = -10, oldX = -10;
/**Position Y du pointeur*/
	private int posY = -10, oldY = -10;
/**Taille du pointeur*/
	private int pointerSize = 5;
/**Collection de points et de droites*/
	
	private ArrayList<Point> points = new ArrayList<Point>();
	private ArrayList<Droite> droites = new ArrayList<Droite>();
	private ArrayList<Cercle> cercle = new ArrayList<Cercle>();
	
	/**Collections qui vont contenir notre situation initial*/
	
	private ArrayList<Point> ini_points = new ArrayList<Point>();
	private ArrayList<Droite> ini_droites = new ArrayList<Droite>();
	private ArrayList<Cercle> ini_cercle = new ArrayList<Cercle>();
	
	private int npd = 1;
	private int a;
	private int b;
	
	private Situation_ini initial =new Situation_ini(ini_points, ini_droites, ini_cercle);

	public DrawPanel(){
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				
				/** On r�cup�re les coordonn�es de la souris et on enl�ve la moiti� de la taille*/
				/** du pointeur pour centrer le trac�*/
				if (pointerType == "droite") {

					if (npd == 1) {

						a = e.getX();
						b = e.getY();
						points.add(new Point(e.getX() - (pointerSize / 2), e.getY() - (pointerSize / 2), pointerSize,
								pointerColor));
						npd = 0;
						repaint();

					} else {

						droites.add(new Droite(
								new Point(a - (pointerSize / 2), b - (pointerSize / 2), pointerSize, pointerColor),
								new Point(e.getX() - (pointerSize / 2), e.getY() - (pointerSize / 2), pointerSize,
										pointerColor)));
						points.add(new Point(e.getX() - (pointerSize / 2), e.getY() - (pointerSize / 2), pointerSize,
								pointerColor));
						npd = 1;
						repaint();

					}

				}
				if (pointerType == "point") {
					points.add(new Point(e.getX() - (pointerSize / 2), e.getY() - (pointerSize / 2), pointerSize,
							pointerColor));
					repaint();
					

				}
				
				if (pointerType == "cercle") {

					if (npd == 1) {

						a = e.getX();
						b = e.getY();
						points.add(new Point(e.getX() - (pointerSize / 2), e.getY() - (pointerSize / 2), pointerSize,
								pointerColor));
						npd = 0;
						repaint();

					} else {

						cercle.add(new Cercle(
								new Point(a - (pointerSize / 2), b - (pointerSize / 2), pointerSize, pointerColor),
								new Point(e.getX() - (pointerSize / 2), e.getY() - (pointerSize / 2), pointerSize,
										pointerColor)));
						npd = 1;
						repaint();

					}

				}

				if (pointerType == "effasser") {
					
					for (Cercle c : cercle) {
						/** On test si on clic sur un des 2 points de la droite si oui on suprime la*/
						/** droite puis le point dans le 2eme test*/
						if ((c.getXp1() - 30 < e.getX() && c.getXp1() + 30 > e.getX() && c.getYp1() - 30 < e.getY()
								&& c.getYp1() + 30 > e.getY())
								|| (c.getXp2() - 30 < e.getX() && c.getXp2() + 30 > e.getX()
										&& c.getYp2() - 30 < e.getY() && c.getYp2() + 30 > e.getY())) {
							cercle.remove(c);
							repaint();
						}
					}

					for (Droite d : droites) {
						/** On test si on clic sur un des 2 points de la droite si oui on suprime la*/
						/** droite puis le point dans le 2eme test*/
						if ((d.getXp1() - 30 < e.getX() && d.getXp1() + 30 > e.getX() && d.getYp1() - 30 < e.getY()
								&& d.getYp1() + 30 > e.getY())
								|| (d.getXp2() - 30 < e.getX() && d.getXp2() + 30 > e.getX()
										&& d.getYp2() - 30 < e.getY() && d.getYp2() + 30 > e.getY())) {
							droites.remove(d);
							repaint();
						}
					}

					for (Point p : points) {
						/** On test si on clic par sur un point, si il en a deja un on le suprime*/
						if ((p.getX() - 30 < e.getX() && p.getX() + 30 > e.getX() && p.getY() - 30 < e.getY()
								&& p.getY() + 30 > e.getY())) {

							points.remove(p);
							repaint();
						}

					}
					
					

				}
			}
		});
	}

	public void paintComponent(Graphics g) {
		

		
		g.setColor(Color.white); /**Rectangle dans lequel on dessine*/
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.blue);
		g.drawLine(800, 0, 800, 900);
		g.drawLine(0, 450, 1600, 450);

		/**on commence par dessiner la situation initial*/

		for (Point p : ini_points) {
			/** On r�cup�re la couleur*/
			g.setColor(Color.red);
			g.fillOval(p.getX(), p.getY(), p.getSize(), p.getSize());

			repaint();
		}
		for (Droite d : ini_droites) {

			g.setColor(Color.red);
			g.drawLine(d.getXp1(), d.getYp1(), d.getXp2(), d.getYp2());

			repaint();
		}
		
		for (Cercle c : ini_cercle) {
			
			int rayon;
			rayon=(int) Math.sqrt((c.getXp1()-c.getXp2())*(c.getXp1()-c.getXp2())+(c.getYp1()-c.getYp2())*(c.getYp1()-c.getYp2()));
			g.setColor(Color.red);
			g.drawOval(c.getXp1()-rayon, c.getYp1()-rayon, 2*rayon, 2*rayon);//(d.getXp1(), d.getYp1(), d.getXp2(), d.getYp2());

			repaint();
		}
		
		
		/** On parcourt nos collection d'objets */
		for (Point p : points) {
			/** On r�cup�re la couleur*/
			g.setColor(Color.black);
			g.fillOval(p.getX(), p.getY(), p.getSize(), p.getSize());

			repaint();
		}
		for (Droite d : droites) {

			g.setColor(Color.black);
			g.drawLine(d.getXp1(), d.getYp1(), d.getXp2(), d.getYp2());

			repaint();
		}
		
		for (Cercle c : cercle) {
			
			int rayon;
			rayon=(int) Math.sqrt((c.getXp1()-c.getXp2())*(c.getXp1()-c.getXp2())+(c.getYp1()-c.getYp2())*(c.getYp1()-c.getYp2()));
			g.setColor(Color.black);
			g.drawOval(c.getXp1()-rayon, c.getYp1()-rayon, 2*rayon, 2*rayon);//(d.getXp1(), d.getYp1(), d.getXp2(), d.getYp2());

			repaint();
		}

	}

/**Efface le contenu*/
	public void erase() {
		points = new ArrayList<Point>();
		droites = new ArrayList<Droite>();
		cercle = new ArrayList<Cercle>();
		repaint();
	}

/**D�finit la couleur du pointeur*/
	public void setPointerColor(Color c) {
		this.pointerColor = c;
	}

/**D�finit la forme du pointeur*/
	public void setPointerType(String str) {
		this.pointerType = str;
	}
	
	public void setSituation(String str) {
		this.situation = str;
	}
	/**Definit la situation initial du probleme*/
	public void Situation() {
		
		this.erase();
		if (situation == "p1")
			initial.situation_1();
		if (situation == "p2")
			initial.situation_2();
		if (situation == "p3")
			initial.situation_3();
		
		
		ini_points=initial.getP();
		ini_droites=initial.getD();
		ini_cercle=initial.getC();
		repaint();
		
	}

}