package vue.repere;

import java.awt.Color;


public class Point {

	  
	  private Color color = Color.black;

	  private int size = 1;  /**taile*/  
	  private int x = -10;
	  private int y = -10;


	  
	  public Point(){}

	  public Point(int x, int y, int size, Color color){
	    this.size = size;
	    this.x = x;
	    this.y = y;
	    this.color = color;
	    
	  }

	  
	  public Color getColor() {
	    return color;
	  }
	  public void setColor(Color color) {
	    this.color = color;
	  }
	  public int getSize() {
	    return size;
	  }
	  public void setSize(int size) {
	    this.size = size;
	  }
	  public int getX() {
	    return x;
	  }
	  public void setX(int x) {
	    this.x = x;
	  }
	  public int getY() {
	    return y;
	  }
	  public void setY(int y) {
	    this.y = y;
	  }
	
	}