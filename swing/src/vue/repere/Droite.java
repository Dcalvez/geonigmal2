package vue.repere;

public class Droite {

	private Point p1;
	private Point p2;

	public Droite(Point _p1, Point _p2) {
		p1 = _p1;
		p2 = _p2;

	}

	public int getXp1() {
		return p1.getX();
	}

	public int getYp1() {
		return p1.getY();
	}

	public int getXp2() {
		return p2.getX();
	}

	public int getYp2() {
		return p2.getY();
	}

}
