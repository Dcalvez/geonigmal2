package vue.repere;

import java.awt.Color;
import java.util.ArrayList;

public class Situation_ini {
	
	/**Une situation initial est definie par une collection de points, de segment et de cercle*/
	private ArrayList<Point> points = new ArrayList<Point>();
	private ArrayList<Droite> droites = new ArrayList<Droite>();
	private ArrayList<Cercle> cercle = new ArrayList<Cercle>();
	
	public Situation_ini(ArrayList<Point> p,ArrayList<Droite> d,ArrayList<Cercle> c) {
		points=p;
		droites=d;
		cercle=c;
		
	}
	
	public ArrayList<Point> getP() {
		return points;
	}
	
	public ArrayList<Droite> getD() {
		return droites;
	}
	
	public ArrayList<Cercle> getC() {
		return cercle;
	}
	
	public void situation_1() {
		
		points = new ArrayList<Point>();
		droites = new ArrayList<Droite>();
		cercle = new ArrayList<Cercle>();
		
		Point a = new Point(500,300,3,Color.black);
		Point b =new Point(90,90,3,Color.black);
		Point c =new Point(800,200,3,Color.black);
		Point d =new Point(100,305,3,Color.black);
		Point e =new Point(500,400,3,Color.black);
		
		
		cercle.add(new Cercle(a,e));
		
	}
	
public void situation_2() {
	
		points = new ArrayList<Point>();
		droites = new ArrayList<Droite>();
		cercle = new ArrayList<Cercle>();
		
		Point a = new Point(60,60,3,Color.black);
		Point b =new Point(90,90,3,Color.black);
		Point c =new Point(800,200,3,Color.black);
		Point d =new Point(100,305,3,Color.black);
		Point e =new Point(600,900,3,Color.black);
		
		points.add(new Point(500,500,3,Color.black));
		
		
		
		
	}

public void situation_3() {
	
	points = new ArrayList<Point>();
	droites = new ArrayList<Droite>();
	cercle = new ArrayList<Cercle>();
	
	Point a = new Point(60,60,3,Color.black);
	Point b =new Point(500,90,3,Color.black);
	Point c =new Point(1500,200,3,Color.black);
	Point d =new Point(100,305,3,Color.black);
	Point e =new Point(600,900,3,Color.black);
	
	droites.add(new Droite(b,c));
	
}

}
