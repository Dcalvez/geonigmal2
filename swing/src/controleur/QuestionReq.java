package controleur;

import java.util.ArrayList;
import connect.JDBCConnect;

public class QuestionReq {
	private ArrayList<String> qtList;
	private ArrayList<ArrayList<String>> repList;
	private ArrayList<ArrayList<String>> brList;
	private JDBCConnect conn = new JDBCConnect("geonigma");
	
	public QuestionReq() {
		qtList = new ArrayList<String>();
		repList = new ArrayList<ArrayList<String>>();
		brList = new ArrayList<ArrayList<String>>();
		qtList = conn.readBDD("texteQuestion", "QUESTION", " ORDER BY idQuestion");

		for(int i = 0; i < qtList.size(); i++) {
			System.out.println("huitre");
			repList.add(conn.readBDD("texteReponse", "REPONSE", " WHERE idQuestion = "+(i)));
			brList.add(conn.readBDD("texteReponse", "REPONSE", " WHERE gdReponse  = 1 AND idQuestion = "+i));
		}
		
		for (ArrayList<String> arrayList : brList) {
			for (String string : arrayList) {
				System.out.print(string+" / ");
			}
			System.out.println("");
		}
		
	}
	
	public ArrayList<String> getQtList() {
		return qtList;
	}
	
	public ArrayList<ArrayList<String>> getRepList() {
		return repList;
	}
	
	public ArrayList<ArrayList<String>> getBrList() {
		return brList;
	}
}
