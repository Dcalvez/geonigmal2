package swing;

import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;
 
@SuppressWarnings("serial")
public class Panneau extends JPanel {
	
	public String txt;
	public int x;
	public int y;
	public int t;
	
	
	public Panneau(String texte, int _x, int _y,int _t) {
		txt=texte;
		x=_x;
		y=_y;
		t=_t;
	}
	
  public void paintComponent(Graphics g){
	  
	  Font font = new Font("Courier", Font.BOLD, t);
	  g.setFont(font);
	  g.drawString(txt, x, y);
  }               
}